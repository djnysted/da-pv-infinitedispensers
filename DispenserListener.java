package leahnto.infinitedispensers;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Dispenser;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.logging.Level;

public class DispenserListener implements Listener {

    InfiniteDispensers infDisp = InfiniteDispensers.getInstance();

    @EventHandler
    public void onDispense(BlockDispenseEvent event) {
        Block dispenser = event.getBlock();
        if( InfiniteDispensers.isInfinite(infDisp, dispenser) ) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(infDisp, () -> {
                ItemStack newItem = event.getItem().clone();
                Dispenser disp = (Dispenser) event.getBlock().getState();
                disp.getInventory().addItem(newItem);
            }, 1L);

        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Block dispenser = event.getBlock();
        if( !(dispenser.getType() == Material.DISPENSER))
        {
            return;
        }
        if( InfiniteDispensers.isInfinite(infDisp, dispenser) ) {
            try {
                infDisp.getData().set("dispensers." + dispenser.getX() + "," + dispenser.getY() + "," + dispenser.getZ() + "," + dispenser.getWorld().getName(), null);
            } catch (NullPointerException e) {
                infDisp.getLogger().log(Level.SEVERE, "Could not delete dispenser from data!");
            }
            infDisp.saveCustomConfigs();
            infDisp.loadDispensers();
        }
    }

    @EventHandler
    public void onOpenDispenser(InventoryOpenEvent event) {
        HumanEntity p = event.getPlayer();
        if ( !( p instanceof Player ) ) {
            return;
        }
        Player player = (Player) p;
        if(event.getInventory().getType().equals(InventoryType.DISPENSER))
        {
            Dispenser dispenser = (Dispenser) event.getInventory().getHolder();
            Block dispBlock = dispenser.getBlock();
            if( InfiniteDispensers.isInfinite( infDisp, dispBlock) && ! (player.hasPermission("infDisp.access"))) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.ITALIC + "" + ChatColor.RED + "You do not have permission to interact with this.");
            }
        }
    }
}
