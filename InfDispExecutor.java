package leahnto.infinitedispensers;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class InfDispExecutor implements CommandExecutor {

    InfiniteDispensers infDisp = InfiniteDispensers.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if ( !(sender instanceof Player ) ) {
            sender.sendMessage("Command must be run by a player.");
            return true;
        }
        Player p = (Player) sender;
        if( args.length < 1 ) {
            p.sendMessage(ChatColor.GREEN + "Use " + ChatColor.DARK_GRAY + "/" + ChatColor.WHITE
                    + "infdisp help " + ChatColor.GREEN + "for more information.");
            return true;
        }
        infDisp.reloadData();
        FileConfiguration dispList;
        dispList = infDisp.getData();
        switch(args[0]) {
            case "set":
                createDisp(p,dispList);
                break;
            case "del":
                deleteDisp(p,dispList);
                break;
            case "status":
                getStatus(p);
                break;
            case "version":
                getVersion(p);
                break;
            case "help":
                getHelpMsg(p);
                break;
            case "info":
                getInfoMsg(p);
                break;
            default:
                p.sendMessage(ChatColor.GREEN + "Command not found. Use " + ChatColor.DARK_GRAY + "/" +
                        ChatColor.WHITE + "infdisp help " + ChatColor.GREEN + "for more information.");
                break;

        }
        infDisp.saveCustomConfigs();
        infDisp.loadDispensers();
        return true;
    }

    // Taken from BlockUtils.getTransparentBlocks(), all credit to them
    @SuppressWarnings("deprecation")
    public static HashSet<Material> getTransparentBlocks() {
        HashSet<Material> transparentBlocks = new HashSet<Material>();
        for (Material material : Material.values()) {
            if (material.isTransparent()) {
                transparentBlocks.add(material);
            }
        }
        return transparentBlocks;
    }

    public void createDisp(Player p, FileConfiguration data) {
        if (! ( p.hasPermission ("infdisp.set"))) {
            return;
        }
        Block lookingAt = p.getTargetBlock(getTransparentBlocks(), 8);
        if (! (lookingAt.getType() == Material.DISPENSER || lookingAt.getType() == Material.DROPPER) ) {
            p.sendMessage(ChatColor.RED + "" + ChatColor.ITALIC + "ERROR" + ChatColor.DARK_GRAY + ":" + ChatColor.WHITE
                    + " Command must be run facing a dispenser/dropper!");
            return;
        }
        if (InfiniteDispensers.isInfinite(infDisp,lookingAt)) {
            p.sendMessage(ChatColor.YELLOW + "This dispenser is already infinite!");
            p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_AMBIENT, 10, 10);
            return;
        }
        data.set("dispensers." + lookingAt.getX() + "," + lookingAt.getY() + "," + lookingAt.getZ() + "," + lookingAt.getWorld().getName(), true);
        p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_HIT,10,10);
        p.sendMessage("Dispenser added.");
    }
    public void deleteDisp(Player p, FileConfiguration data) {
        if (! ( p.hasPermission ("infdisp.del"))) {
            return;
        }
        Block lookingAt = p.getTargetBlock(getTransparentBlocks(), 8);
        if (! (lookingAt.getType() == Material.DISPENSER || lookingAt.getType() == Material.DROPPER) ) {
            p.sendMessage(ChatColor.RED + "" + ChatColor.ITALIC + "ERROR" + ChatColor.DARK_GRAY + ":" + ChatColor.WHITE
                    + " Command must be run facing a dispenser/dropper!");
            return;
        }
        if (InfiniteDispensers.isInfinite(infDisp,lookingAt)) {
            try {
                data.set("dispensers." + lookingAt.getX() + "," + lookingAt.getY() + "," + lookingAt.getZ() + "," + lookingAt.getWorld().getName(), null);
                p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_HIT, 10, 10);
                p.sendMessage("Dispenser removed.");
            } catch (NullPointerException e) {
                p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Could not remove InfiniteDispenser");
                p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_AMBIENT, 10, 10);
            }
        }
    }
    public void getStatus(Player p)  {
        if (! ( p.hasPermission ("infdisp.status"))) {
            return;
        }
        Block lookingAt = p.getTargetBlock(getTransparentBlocks(), 8);
        if (! (lookingAt.getType() == Material.DISPENSER || lookingAt.getType() == Material.DROPPER) ) {
            p.sendMessage(ChatColor.RED + "" + ChatColor.ITALIC + "ERROR" + ChatColor.DARK_GRAY + ":" + ChatColor.WHITE
                    + " Command must be run facing a dispenser/dropper!");
        }
        if (InfiniteDispensers.isInfinite(infDisp,lookingAt)) {
            p.sendMessage(ChatColor.GREEN + "This is an infinite dispenser/dropper!");
            p.playSound(p.getLocation(),Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 10);
        } else {
            p.sendMessage(ChatColor.GREEN + "This is " + ChatColor.RED + "not " + ChatColor.GREEN + "an infinite dispenser/dropper!");
        }
    }
    public void getVersion(Player p) {
        if (! ( p.hasPermission ("infdisp.version"))) {
            return;
        }
        p.sendMessage(ChatColor.BLUE + "Currently running version " + ChatColor.WHITE + InfiniteDispensers.getVersion());
    }
    public void getHelpMsg(Player p) {
        if (! ( p.hasPermission ("infdisp.help"))) {
            return;
        }
        p.sendMessage(ChatColor.BLUE + "Commands List:\n" + ChatColor.DARK_PURPLE + "-" + ChatColor.DARK_GRAY + "/"
                        + ChatColor.WHITE + "infdisp help" + ChatColor.DARK_GRAY + ": " + ChatColor.GREEN + "Displays this menu.\n" +
                        ChatColor.DARK_PURPLE + "-" + ChatColor.DARK_GRAY + "/" + ChatColor.WHITE + "infdisp set" +
                        ChatColor.DARK_GRAY + ": " + ChatColor.GREEN + "Creates an InfiniteDispenser out of a targeted dispenser.\n" +
                        ChatColor.DARK_PURPLE + "-" + ChatColor.DARK_GRAY + "/" + ChatColor.WHITE + "infdisp del" +
                        ChatColor.DARK_GRAY + ": " + ChatColor.GREEN + "Deletes a targeted InfiniteDispenser.\n" +
                        ChatColor.DARK_PURPLE + "-" + ChatColor.DARK_GRAY + "/" + ChatColor.WHITE + "infdisp status" +
                        ChatColor.DARK_GRAY + ": " + ChatColor.GREEN + "Checks if a targeted dispenser is an InfiniteDispenser.\n" +
                        ChatColor.DARK_PURPLE + "-" + ChatColor.DARK_GRAY + "/" + ChatColor.WHITE + "infdisp version" +
                        ChatColor.DARK_GRAY + ": " + ChatColor.GREEN + "Gets plugin version information.\n" +
                        ChatColor.DARK_PURPLE + "-" + ChatColor.DARK_GRAY + "/" + ChatColor.WHITE + "infdisp info" +
                        ChatColor.DARK_GRAY + ": " + ChatColor.GREEN + "Gets additional information and disclaimers.");
    }
    public void getInfoMsg(Player p) {
        if (! ( p.hasPermission ("infdisp.info"))) {
            return;
        }
        p.sendMessage(ChatColor.GREEN + "Simple plugin which allows dispensers and droppers to dispense infinite items" +
                "\nUsage can be found under " + ChatColor.DARK_GRAY + "/" + ChatColor.WHITE + "infdisp help" + ChatColor.GREEN +
                ".\nFor more information contact " + ChatColor.GRAY + "djnysted#1871 " + ChatColor.GREEN + "or " + ChatColor.GRAY +
                "legato#0808" + ChatColor.GREEN + " on Discord.\nBy using this plugin you acknowledge any/all bugs or issues" +
                " that may arise.\n" + ChatColor.RED + "Do not redistribute without permission.");
    }

}
