package leahnto.infinitedispensers;

import org.bukkit.block.Block;
import org.bukkit.block.Dispenser;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;

public final class InfiniteDispensers extends JavaPlugin {

    private File infDispDataFile = null;
    private File infDispConfigFile = null;
    private FileConfiguration infDispData = null;
    private FileConfiguration infDispConfig = null;
    private final static String version = "1.0.0";
    private static InfiniteDispensers instance;
    private ArrayList<String> dispensers = new ArrayList();

    @Override
    public void onEnable() {
        instance = this;
        reloadConfiguration();
        reloadData();
        loadDispensers();
        getServer().getPluginCommand("infdisp").setExecutor( new InfDispExecutor() );
        getServer().getPluginManager().registerEvents( new DispenserListener(), this );

    }

    public void loadDispensers() {
        ArrayList<String> dispenserList = new ArrayList();
        ConfigurationSection dispense = getData().getConfigurationSection("dispensers");
        if ( dispense == null) {
            return;
        }
        for(String s : dispense.getKeys(false))
        {
            dispenserList.add(s);
        }
        this.dispensers = dispenserList;
    }

    public void reloadConfiguration() {
        if ( infDispConfigFile == null ) {
            infDispConfigFile = new File(getDataFolder(), "config.yml");
        }
        infDispConfig = YamlConfiguration.loadConfiguration(infDispConfigFile);
        if ( !infDispConfig.contains("allDispInf") ) {
            infDispConfig.set( "allDispInf" , false);
        }
    }

    public void reloadData() {
        if ( infDispDataFile == null ) {
            infDispDataFile = new File(getDataFolder(), "dispenserList.yml");
        }
        infDispData = YamlConfiguration.loadConfiguration(infDispDataFile);
    }

    public static boolean isInfinite(InfiniteDispensers instance, Block dispenser) {
        if ( (boolean) instance.infDispConfig.get("allDispInf") ) {
            return true;
        }
        Dispenser disp = (Dispenser) dispenser.getState();
        if ( (disp.getCustomName() != null && disp.getCustomName().equalsIgnoreCase("Infinite Dispenser")) ) {
            return true;
        }
        String key = "" + disp.getX() + "," + disp.getY() + "," + disp.getZ() + "," + disp.getWorld().getName();
        if ( instance.dispensers.contains(key) )
        {
            return true;
        }
        return false;
    }

    public FileConfiguration getData() {
        if ( infDispData == null ) {
            reloadData();
        }
            return infDispData;
    }

    public FileConfiguration getConfig() {
        if ( infDispConfig == null ) {
            reloadConfiguration();
        }
            return infDispConfig;
    }

    public static InfiniteDispensers getInstance() { return instance; }

    public static String getVersion() {
        return version;
    }

    public void saveCustomConfigs() {
        getLogger().log(Level.INFO, "Saving configurations...");
        if (infDispConfig == null || infDispConfigFile == null || infDispData == null || infDispConfigFile == null) {
            return;
        }
        try {
            getData().save(infDispDataFile);
        } catch (IOException ex) {
            getLogger().log(Level.SEVERE, "Could not save config to " + infDispDataFile, ex);
        }
        try {
            getConfig().save(infDispConfigFile);
        } catch (IOException ex) {
            getLogger().log(Level.SEVERE, "Could not save config to " + infDispConfigFile, ex);
        }
    }

    @Override
    public void onDisable() {
        saveCustomConfigs();
    }


}
